<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Udeita</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a data-toggle="modal" href='#modal-id' class=""><i class="fa fa-star text-warning fa-btn"></i> Información</a>
        </li>
        <li>
          <a data-toggle="modal" href='#modal-contact' class=""><i class="fa fa-envelope text-warning fa-btn"></i> Contacto</a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div>
</nav>

<div class="modal fade" id="modal-id">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Información</h4>
      </div>
      <div class="modal-body">
        <p>&bull; La información fue obtenida a través de diversos documentos del Consejo Nacional de Educación y tiene un carácter de pública. Los datos entregados y la veracidad de la información es responsabilidad de las distintas instituciones. Los datos no fueron manipulados de ninguna manera.</p>
        <p>&bull; Los gráficos son representativos (no como otros del gobierno) y reflejan de buena manera lo que ocurre a nivel país en la Educación Superior.</p>
        <p>&bull; Existen carreras que si bien están presentes, no cuentan con toda la información.</p>
        <p>&bull; Existe la posibilidad de buscar carreras Online. Sólo busca en comuna "Sede Virtual Online" y en jornada elige "Otro".</p>
        <p>&bull; No se ocuparon bolsas haciendo esta aplicación, pero sí hubo mucho café y tabletones.</p><hr>
        <p class="text-center">¡Pronto estaremos con nuevas actualizaciones!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-contact">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title text-center">¡Contáctame!</h3>
      </div>
      <div class="modal-body">
        <form method="post" action="https://formspree.io/hola@udeita.cl" role="form">
          <input type="hidden" name="_language" value="es" />
          <div class="header header-primary text-center">
              <h4 class="title title-up">Si tienes dudas, encontraste errores o quieres ver alguna otra información en la página, coméntame. Trataré de responder lo más rápido posible.</h4><hr>       
          </div>
          <div class="content">
              <div class="input-group form-group-no-border">
                  <span class="input-group-addon">
                      <i class="fa fa-user"></i>
                  </span>
                  <input required type="text" name="nombre" id="name" class="form-control" placeholder="Nombre">
              </div>
              <div class="input-group form-group-no-border">
                  <span class="input-group-addon">
                      <i class="fa fa-envelope"></i>
                  </span>
                  <input required type="text" class="form-control" name="_replyto" id="email" placeholder="Email">
              </div>
              <div class="textarea-container">
                  <textarea required class="form-control" name="mensaje" id="message" rows="4" cols="80" placeholder="Escribe tu mensaje"></textarea>
              </div>
          </div>
          <div>
              <p class="text-center"><button value="Send" type="submit" class="btn btn-success"><i class="fa fa-send-o fa-btn"></i>Enviar</button></p>
          </div>
      </form>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>