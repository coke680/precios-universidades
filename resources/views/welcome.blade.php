<!DOCTYPE html>
<?php header('Access-Control-Allow-Origin: *'); ?>
<html lang="en">
  <head>

    <meta property="og:description" content="¡En Udeita podrás conocer la información de tu carrera! Compara los datos de tu carrera que presentan las distintas instituciones en el presente año (2018)" />        
    <meta property="og:url"content="http://www.udeita.cl/" />        
    <meta property="og:title" content="Udeita. Conociendo la información unificada de las universidades." />
      
      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124738919-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-124738919-1');
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="public/img/icon.png"/>
    <link rel="shortcut icon" type="image/png" href="public/img/icon.png"/>

    <title>{{ request()->get('c') }} en {{ request()->get('q') }} - BETA</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">    
    <!-- Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css" rel="stylesheet"/>
  </head>

  <body>

    @include('navbar.index')

    <div class="modal fade" id="modal-id">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Información</h4>
          </div>
          <div class="modal-body">
            <p>La información fue obtenida a través de diversos documentos del Consejo Nacional de Educación y tiene un carácter de pública. Los datos entregados y la veracidad de la información es responsabilidad de las distintas instituciones. Los datos no fueron manipulados de ninguna manera.</p>
            <p>Los gráficos son representativos (no como otros del gobierno) y reflejan de buena manera lo que ocurre a nivel país en la Educación Superior.</p>
            <p>No se ocuparon bolsas haciendo esta aplicación, pero sí hubo mucho café.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <div class="jumbotron jumbotron-back-welcome text-center">
      <div class="container">
        <h2>{{ request()->get('c') }} en  
            @if (request()->get('q') == 'todas')
              todas las comunas del país
            @else
              {{ request()->get('q') }} 
            @endif
            <span class="subtitle">({{ request()->get('j') }})</span></h2>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="x_panel">
          <div class="x_content">
            <form class="form-inline form-careers" action="" method="GET" role="form">
              <div class="form-group col-lg-2 col-xs-12">
                  <select class="form-control select-form" id="t" name="t" required="required">
                    <option value="">Tipo de Institución</option>
                    <option value="IP - CFT">IP - CFT</option>
                    <option value="Universidad">Universidad</option>
                  </select>

                  @if ($errors->has('t'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('t') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group col-lg-3 col-xs-12">
                  <select class="form-control select-form" id="q" name="q" required="required">
                    <option value="">Seleccione una comuna</option>
                  </select>

                  @if ($errors->has('q'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('q') }}</strong>
                      </span>
                  @endif
              </div>
              
              <div class="form-group col-lg-3 col-xs-12">
                  <select class="form-control select-form" name="c" id="c" required="required">
                    <option value="">Seleccione una carrera</option>
                  </select>

                  @if ($errors->has('c'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('c') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="form-group col-lg-2 col-xs-12">
                  <select class="form-control select-form" id="j" name="j" required="required">
                    <option value="">Jornada</option>
                    <option value="Diurno">Diurno</option>
                    <option value="Otro">Otro</option>
                    <option value="Vespertino">Vespertino</option>
                  </select>

                  @if ($errors->has('j'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('j') }}</strong>
                      </span>
                  @endif
              </div>

              <div class="col-lg-2 col-xs-12">
                <button type="submit" class="btn btn-success btn-submit" style="margin-bottom: 0px;">Verificar <i class="fa fa-check"></i></button>
              </div>              

            </form>
          </div>
        </div>
      </div>

      @if(count($data_chart) == 0)
          <div class="row">          
            <div class="jumbotron text-center">
              <h2 class="display-3">{{ request()->get('c') }} en {{ request()->get('q') }} <span class="">({{ request()->get('j') }})</span> no fue encontrada :(</h2>
              <p class="lead">Prueba con otra combinación.</p>
            </div>
          </div>
      @else

        <table id="datatable" class="hidden">
          <thead>
              <tr>
                  <th></th>
                  <th>Universidades</th>
              </tr>
          </thead>
          <tbody>
              @foreach ($data_chart as $data)
              <tr>
                <th class="text-center">
                    {{ $data->nombre_institucion }}
                </th>
                <th class="text-center">
                    {{ $data->valor_arancel }}
                </th>
              </tr>
              @endforeach        
          </tbody>
        </table>        

        <div class="table-responsive graphs">
            <h3 class="text-center">Tabla informativa</h3><hr>
            <table class="table table-hover table-bordered" id="table">
                <thead>
                    <tr>
                    @foreach ($data_chart as $data)
                        <th class="text-center">
                            {{ $data->nombre_institucion }}
                        </th>
                    @endforeach
                    </tr>
                </thead>
                <tbody class="text-center">
                    <tr>
                    @foreach ($data_chart as $data)
                        <td>
                            <strong>Arancel REAL 2018:</strong> <span class="">${{ number_format(round($data->valor_arancel),0,",",".") }} pesos</span><br><br>
                            <strong><i class="fa fa-users fa-btn"></i> Estudiantes nuevos 2018:</strong> <span class="">{{ $data->matricula_primer }}</span><br><br>
                            <strong><i class="fa fa-users fa-btn"></i> Último seleccionado 2018:</strong> <span class="">{{ $data->ultimo_corte_psu }} puntos</span><br><br>
                            <strong><i class="fa fa-clock-o fa-btn"></i> Duración:</strong> <span class="">{{ $data->duracion }} semestres</span><br><br>
                            <strong><i class="text-success fa fa-money fa-btn"></i> Valor del Título:</strong> <span class="">${{ number_format($data->valor_titulo ,0,",",".") }} pesos</span><br><br>
                            <strong><i class="text-success fa fa-money fa-btn"></i> Valor matrícula:</strong> <span class="">${{ number_format($data->valor_matricula ,0,",",".") }} pesos</span><br><br>
                            <strong>¿Gratuidad?: </strong><span class="">${{ $data->clasificacion_5 }}</span><br><br>
                            <strong><span class="">{{ $data->clasificacion_3 }}</span></strong><br><br>
                          </td>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-lg-6 col-xs-12 graphs-line graphs">
                  <div id="heatmap" style="height:550px; width: 100%;"></div>
            </div>
            <div class="col-lg-6 col-xs-12 graphs-barchart graphs">
                  <div id="barchart" style="height:550px; width: 100%;"></div>
            </div>
        </div>

        <div class="text-center row section-info">
            <div class="col-lg-6">
                <div class="lead bg-grey"><strong>Si tienes CAE</strong>, o pretendes pedirlo para financiar tus estudios, accede al simulador de cuotas y revisa el monto aproximado a pagar al final de tu carrera. Recordamos que los valores se encuentran en UF.<hr>
                  <p class="lead">
                    <a class="btn btn-primary btn-md" target="_blank" href="http://simulador.ingresa.cl/" role="button"><i class="fa fa-eye fa-btn"></i> Ver simulador</a>
                  </p>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="lead bg-grey"><strong>Conoce la empleabilidad</strong>, el ingreso promedio y algunos indicadores de la carrera que escogiste. Esto te ayudará a tener mayor claridad acerca de lo que pasará una vez terminada la etapa universitaria.<hr>
                  <p class="lead">
                    <a class="btn btn-info btn-md" target="_blank" href="http://www.mifuturo.cl/index.php/futuro-laboral/buscador-por-carrera-d-institucion" role="button"><i class="fa fa-bar-chart fa-btn"></i> Ver indicadores</a>
                  </p>
                </div>
            </div>   
        </div>

        <section class="text-center jumbotron">
          <div class="row">
              <div class="col-lg-6">
                  <h3>¿Tienes comentarios acerca de {{ request()->get('c') }}?<br> ¡A los nuevos alumnos les encantará conocerlas!</h3><hr>
              </div>  
              <div class="col-lg-6 col-xs-12">
                  <form action="{{ url('comments') }}" method="POST" role="form" id="form-comment">
                      {{ csrf_field() }}                  
                      <div class="form-group">
                          <select class="form-control select-form" id="institucion" name="institucion" required="required">
                              <option value="">Selecciona la Institución</option>
                              @foreach ($data_chart as $data)
                              <option value="{{ $data->nombre_institucion}}">{{ $data->nombre_institucion}}</option>
                              @endforeach
                          </select>
                          <input class="hidden" value="{{ request()->get('q') }}" id="city" name="city">
                          <input class="hidden" value="{{ request()->get('c') }}" id="career" name="career">
                          <input class="hidden" value="{{ request()->get('j') }}" id="time" name="time">                      
                      </div>
                      <div class="form-group">                        
                          <textarea type="text" class="form-control" name="comment" id="comment" placeholder="Ingresa tu comentario de {{ request()->get('c') }}" required="required"></textarea>
                          @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif 
                      </div><hr>
                      <button type="submit" id="submit-form-comment" class="btn btn-success">
                          <span class="text-send">
                              <i class="fa fa-btn fa-send"></i>Enviar
                          </span>
                          <span class="text-sent hidden">
                              <i class="fa fa-spinner fa-spin fa-btn"></i> Enviando comentario...
                          </span>
                      </button>
                  </form>
              </div>
                      
          </div>
        </section>

       @endif
    </div>

    @include('footer.index')

      <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/3.7.0/echarts.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="js/jquery.loading.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/heatmap.js"></script>
    <script src="https://code.highcharts.com/modules/treemap.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    @if (Session::has('success'))
        <script type="text/javascript">
            toastr.success("{{ Session::get('success') }}");
        </script>
    @endif 

    <script type="text/javascript">
      $(document).ready(function() {

          $('.btn-submit').click(function(){
              if ($( "#q" ).val() && $( "#t" ).val() && $( "#c" ).val() && $( "#j" ).val()) {  
                  $("body").loading({
                      theme: 'dark',
                      message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Consultando...</h3>'
                  });   
              }
          });

          $('#submit-form-comment').click(function(){
              if ($( "#institucion" ).val() && $( "#comment" ).val()) {
                $(this).addClass('disabled');
                $('.text-send').addClass('hidden');
                $('.text-sent').removeClass('hidden');
              }
          });           

          $(".select-form").select2({  width: '100%' }); 
          
          $(function() {
              $("#t").val('{{ request()->get('t') }}').change();
              $("#j").val('{{ request()->get('j') }}').change();
          });            
          
          $('#t').on('change', function(event) {
              
              $("body").loading({
                  theme: 'dark',
                  message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Buscando comunas...</h3>'
              });

              $.ajax({
                  url: 'getDataPrograma',
                  type: 'GET',
                  data: {tipo_institucion: $(this).val()},
              })
              .done(function(response) {
                  var options = ''
                  options += `<option value="">Seleccione una comuna</option><option value="todas">Todas las comunas</option><option value='-' disabled>―――――――――――――</option>`
                  $.each(response, function(key, value){
                      options += `<option value="${value.nombre_sede}">${value.nombre_sede}</option>`
                  })
                  $('#q').html(options);
                  $("#q").val('{{ request()->get('q') }}').change();
                  $("body").loading('stop');                    
              })
              .fail(function() {
                  console.log("error");
              })
          });

          $('#q').on('change', function(event) {
              
              $("body").loading({
                  theme: 'dark',
                  message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Buscando carreras...</h3>'
              });

              $.ajax({
                  url: 'getData',
                  type: 'GET',
                  data: {comuna: $(this).val(), tipo_institucion: $('#t').val()},
              })
              .done(function(response) {
                  var options = ''
                  options += `<option value="" selected>Seleccione una carrera</option>`
                  $.each(response, function(key, value){
                      options += `<option value="${value.nombre_programa}">${value.nombre_programa}</option>`
                  })
                  $('#c').html(options);
                  $("#c").val('{{ request()->get('c') }}').change();
                  $("body").loading('stop');
              })
              .fail(function() {
                  console.log("error");
              })
          });
              

        if( typeof (echarts) === 'undefined'){ return; }
        // console.log('init_echarts');  
    
        var data_graph = <?php echo json_encode($data_chart); ?>;
        data_final = [];  
        $.each(data_graph, function(key, value){

            nombre_sede = {
                text: value.nombre_sede,
                subtext: value.nombre_programa
            }

            dataset = {
                name: value.nombre_institucion,
                type: 'scatter',
                tooltip: {
                    trigger: 'item',
                    formatter: function(params) {
                      if (params.value.length > 1) {
                      return params.seriesName + ' :<br/>' + params.value[0] + ' matriculados. <br>' + Math.round(params.value[1]) + ' pesos. ';
                      } else {
                      return params.seriesName + ' :<br/>' + params.name + ' : ' + Math.round(params.value) + 'P ';
                      }
                    }
                },
                data: [[value.matricula_primer, value.valor_arancel]],
                markPoint: {
                    data: [{
                      type: 'max',
                      name: 'Max'
                    }, {
                      type: 'min',
                      name: 'Arancel'
                    }]
                },
                markLine: {
                    data: [{
                      type: 'average',
                      name: 'Arancel'
                    }]
                }
            }
            data_final.push(dataset)
        });

        /*Highcharts*/
        Highcharts.chart('barchart', {
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Distribución de aranceles 2018'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Valor del arancel en miles.'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        formatter:function() {
                            return this.point.y;
                        }
                    }
                }
            },
            exporting: { enabled: false },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/> $' +
                        this.point.y + '<br>' + this.point.name.toUpperCase();
                }
            }
        });

        var data_heatmap = <?php echo json_encode($data_chart); ?>;
        data_final_heatmap = [];        
        i = 1;
        sum_values = 0;

        $.each(data_heatmap, function(key, value){
            data_chartheat = {
                name: value.nombre_institucion,
                value: parseInt(value.matricula_primer),
                colorValue: i
            }
            i = i + 1;
            sum_values = sum_values + parseInt(value.matricula_primer);
            data_final_heatmap.push(data_chartheat)
        });

        //HEATMAP HIGHCHART
        Highcharts.chart('heatmap', {
            colorAxis: {
                minColor: '#f5f5f5',
                maxColor: Highcharts.getOptions().colors[0]
            },
            exporting: { enabled: false },
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data: data_final_heatmap
            }],
            plotOptions: {
                treemap: {
                    dataLabels: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b><br>' +
                                '<b style="font-size: 20px;text-align: center;">' + ((this.point.value / sum_values) * 100).toFixed(1) + " %" + '</b> ' +
                                '<br/>';
                        },
                    }
                }
            },
            title: {
                text: 'Porcentaje de estudiantes por institución <br>(Matriculas primer año)'
            }
        });

        if ($('#echart_scatter').length ){ 

            var echartScatter = echarts.init(document.getElementById('echart_scatter'));

            echartScatter.setOption({
            tooltip: {
                trigger: 'axis',
                showDelay: 0,
                axisPointer: {
                    type: 'cross',
                    lineStyle: {
                      type: 'dashed',
                      width: 1
                    }
                }
            },
            legend: {
              top: 0,
              right: 80,
              data: ['C.F.T. ACUARIO DATA','C.F.T. ALEMÁN DE VIÑA DEL MAR','C.F.T. ALFA','C.F.T. ALPES','C.F.T. ANDRÉS BELLO','C.F.T. AQUATECH','C.F.T. ASPRO','C.F.T. AUSTRAL','C.F.T. BARROS ARANA','C.F.T. CÁMARA DE COMERCIO DE SANTIAGO','C.F.T. CEDEP','C.F.T. CEDUC - UCN','C.F.T. CEFONOR','C.F.T. CEITEC','C.F.T. CENAFOM','C.F.T. CENCO','C.F.T. CENTRO DE ENS. ALTA COSTURA PAULINA DIARD','C.F.T. CENTRO DE EST. PARAMÉDICOS DE STGO. CEPSA','C.F.T. CENTRO TECNOLÓGICO SUPERIOR INFOMED','C.F.T. CEPA DE LA III REGIÓN','C.F.T. CEPONAL','C.F.T. CHILENO-NORTEAMERICANO','C.F.T. CIARTES','C.F.T. CIMA RENGO','C.F.T. COLCHAGUA','C.F.T. CRECIC','C.F.T. CROWNLIET','C.F.T. DE EST. SUP.Y CAPACITACIÓN PROFESIONAL LAPLACE','C.F.T. DE LA INDUSTRIA GRÁFICA O C.F.T.  INGRAF','C.F.T. DE TARAPACÁ','C.F.T. DEL MEDIO AMBIENTE','C.F.T. DIEGO PORTALES','C.F.T. DUOC UC','C.F.T. ECATEMA','C.F.T. EDUCAP','C.F.T. EL ROBLE','C.F.T. ENAC','C.F.T. ESANE DEL NORTE','C.F.T. ESC. ALTOS EST. DE LA COMUNICACIÓN EACE','C.F.T. ESC. DE ARTES APLICADAS OFICIOS DEL FUEGO','C.F.T. ESC. DE INTÉRPRETES INCENI','C.F.T. ESC. SUP. DE ADM. DE NEGOCIOS - ESANE','C.F.T. ESC. SUPERIOR DE COMERCIO EXTERIOR','C.F.T. ESCUELA CULINARIA FRANCESA','C.F.T. ESPERANZA JOVEN','C.F.T. ESTUDIO PROFESOR VALERO','C.F.T. ESUCOMEX','C.F.T. FINNING','C.F.T. FONTANAR','C.F.T. ICADE','C.F.T. ICEL','C.F.T. IFE ESCUELA DE NEGOCIOS','C.F.T. IGNACIO DOMEYKO','C.F.T. INACAP','C.F.T. INCOR','C.F.T. INSEC','C.F.T. INST. CENTRAL DE CAPACITACIÓN EDUCACIONAL ICCE','C.F.T. INST. CHILENO ALEMÁN DE CULTURA DE SAN ANTONIO','C.F.T. INST. INTEC','C.F.T. INST. SUPERIOR DE ELECTRÓNICA GAMMA','C.F.T. INST. SUPERIOR DE ESTUDIOS JURÍDICOS CANON','C.F.T. INSTITUTO CHILENO-BRITÁNICO DE CONCEPCIÓN','C.F.T. INSTITUTO SUPERIOR ALEMÁN DE COMERCIO INSALCO','C.F.T. INSTITUTO TECNOLÓGICO DE CHILE','C.F.T. INTECTUR','C.F.T. IPROSEC','C.F.T. ITPUCH INST. POLITÉCNICO DE LA U. DE CHILE',,'C.F.T. ITUR','C.F.T. JAVIERA CARRERA','C.F.T. JOHN F. KENNEDY','C.F.T. JORGE ALVAREZ ECHEVERRÍA','C.F.T. JUAN BOHON','C.F.T. LA ARAUCANA','C.F.T. LOS FUNDADORES','C.F.T. LOS LAGOS','C.F.T. LOS LEONES','C.F.T. LOTA-ARAUCO','C.F.T. LUIS ALBERTO VERA','C.F.T. MAGNOS','C.F.T. MANPOWER','C.F.T. MASSACHUSETTS','C.F.T. MAULE','C.F.T. OSORNO','C.F.T. PROANDES','C.F.T. PRODATA','C.F.T. PROFASOC','C.F.T. PROTEC','C.F.T. PUKARÁ','C.F.T. SALESIANOS DON BOSCO','C.F.T. SAN AGUSTÍN DE TALCA','C.F.T. SAN ALONSO','C.F.T. SANTO TOMÁS','C.F.T. SIMÓN BOLIVAR','C.F.T. SOEDUC LA LIGUA','C.F.T. TECCON','C.F.T. TEODORO WICKEL','C.F.T. U. VALPO.','C.F.T. UCEVALPO','C.F.T. UDA','C.F.T. UTEM','C.F.T. ZIPTER','I.P. ADVENTISTA','I.P. AGRARIO ADOLFO MATTHEI','I.P. AIEP','I.P. ALEMÁN WILHELM VON HUMBOLDT','I.P. ALPES','I.P. CÁMARA DE COMERCIO DE SANTIAGO','I.P. CAMPVS','I.P. CARLOS CASANUEVA','I.P. CENAFOM','I.P. CHILENO BRITÁNICO','I.P. CHILENO NORTEAMERICANO','I.P. CIISA','I.P. CS. DE LA COMPUTACIÓN ACUARIO DATA','I.P. DE ARTES ESCENICAS KAREN CONNOLLY','I.P. DE ARTES Y COMUNICACIÓN ARCOS','I.P. DE CHILE','I.P. DE CIENCIAS Y ARTES INCA-CEA','I.P. DE LOS ANGELES','I.P. DE LOS LAGOS','I.P. DE MÚSICA DE SANTIAGO','I.P. DEL VALLE CENTRAL','I.P. DIEGO PORTALES','I.P. DUOC UC','I.P. EATRI','I.P. ENAC','I.P. ESC. DE CONTADORES AUDITORES DE STGO.','I.P. ESCUELA DE CINE DE CHILE','I.P. ESCUELA DE MARINA MERCANTE PILOTO PARDO','I.P. ESCUELA MODERNA DE MÚSICA','I.P. ESUCOMEX','I.P. GUILLERMO SUBERCASEAUX','I.P. HELEN KELLER','I.P. HOGAR CATEQUÍSTICO','I.P. IACC','I.P. INACAP','I.P. INSTITUTO NACIONAL DEL FÚTBOL','I.P. INSTITUTO SUPERIOR DE ELECTRÓNICA GAMMA','I.P. INTERNACIONAL DE ARTES CULINARIAS Y SERVICIOS','I.P. IPEGE','I.P. LA ARAUCANA','I.P. LATINOAMERICANO DE COMERCIO EXTERIOR','I.P. LIBERTADOR DE LOS ANDES','I.P. LOS LEONES','I.P. PROJAZZ','I.P. PROVIDENCIA','I.P. SANTO TOMÁS','I.P. TEATRO LA CASA','I.P. VERTICAL','I.P. VIRGINIO GÓMEZ','PONTIFICIA U. CATÓLICA DE CHILE','PONTIFICIA U. CATÓLICA DE VALPARAÍSO','U. ACADEMIA DE HUMANISMO CRISTIANO','U. ADOLFO IBÁÑEZ','U. ADVENTISTA DE CHILE','U. ALBERTO HURTADO','U. ANDRÉS BELLO','U. ARTURO PRAT','U. AUSTRAL DE CHILE','U. AUTÓNOMA DE CHILE','U. BERNARDO O`HIGGINS','U. BOLIVARIANA','U. CATÓLICA CARDENAL RAÚL SILVA HENRÍQUEZ','U. CATÓLICA DE LA SANTÍSIMA CONCEPCIÓN','U. CATÓLICA DE TEMUCO','U. CATÓLICA DEL MAULE','U. CATÓLICA DEL NORTE','U. CENTRAL DE CHILE','U. CHILENO-BRITÁNICA DE  CULTURA','U. DE ACONCAGUA','U. DE ANTOFAGASTA','U. DE ARTE Y CIENCIAS SOCIALES ARCIS','U. DE ARTES, CIENCIAS Y COMUNICACIÓN UNIACC','U. DE ATACAMA','U. DE AYSEN','U. DE CHILE','U. DE CONCEPCIÓN','U. DE LA FRONTERA','U. DE LA SERENA','U. DE LAS AMÉRICAS','U. DE LOS ANDES','U. DE LOS LAGOS','U. DE MAGALLANES','U. DE O`HIGGINS','U. DE PLAYA ANCHA DE CIENCIAS DE LA EDUCACIÓN','U. DE RANCAGUA','U. DE SANTIAGO DE CHILE','U. DE TALCA','U. DE TARAPACÁ','U. DE VALPARAÍSO','U. DE VIÑA DEL MAR','U. DEL BÍO-BÍO','U. DEL DESARROLLO','U. DEL MAR','U. DEL PACÍFICO','U. DIEGO PORTALES','U. EUROPEA DE NEGOCIOS','U. FINIS TERRAE','U. GABRIELA MISTRAL','U. IBEROAMERICANA DE CIENCIAS Y TECNOLOGÍA','U. LA ARAUCANA','U. LA REPÚBLICA','U. LOS LEONES','U. MARÍTIMA DE CHILE','U. MAYOR','U. METROPOLITANA DE CIENCIAS DE LA EDUCACIÓN','U. MIGUEL DE CERVANTES','U. PEDRO DE VALDIVIA','U. REGIONAL SAN MARCOS','U. SAN SEBASTIÁN','U. SANTO TOMÁS','U. SEK','U. TÉCNICA FEDERICO SANTA MARÍA','U. TECNOLÓGICA DE CHILE INACAP','U. TECNOLÓGICA METROPOLITANA','U. UCINF']
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {
                        show: true,
                        title: "Guardar imagen"
                    }
                }
            },
            xAxis: [{
                type: 'value',
                scale: true,
                axisLabel: {
                    formatter: '{value}'
                }
            }],
            yAxis: [{
              type: 'value',
              scale: true,
              axisLabel: {
                  formatter: '$ {value}'
              }
            }],
            series: data_final
            });
          }          
      });
    </script>
  </body>
</html>
