<!DOCTYPE html>
<?php header('Access-Control-Allow-Origin: *'); ?>
<html lang="en">
  <head>

    <meta property="og:description" content="¡En Udeita podrás conocer la información de tu carrera! Compara los datos de tu carrera que presentan las distintas instituciones en el presente año (2018)" />        
    <meta property="og:url"content="http://www.udeita.cl/" />        
    <meta property="og:title" content="Udeita. Conociendo la información unificada de las universidades." />
      
      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124738919-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-124738919-1');
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="public/img/icon.png"/>
    <link rel="shortcut icon" type="image/png" href="public/img/icon.png"/>

    <title>Información de Universidades - BETA</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">    
    <!-- Bootstrap -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css" rel="stylesheet"/>
  </head>

  <body>

    @include('navbar.index')

    <div class="jumbotron jumbotron-back text-center">
      <div class="container">
        <h1>¡En Udeita podrás conocer la información de tu carrera!</h1>
        <p>Compara los datos de tu carrera que presentan las distintas instituciones en el presente año (2018)</p>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="x_panel">
          <div class="x_content">
            <form class="form-inline form-careers" action="" method="GET" role="form">

              <div class="form-group col-lg-2 col-xs-12">
                  <select class="form-control select-form" id="t" name="t" required="required">
                    <option value="">Tipo de Institución</option>
                    <option value="IP - CFT">IP - CFT</option>
                    <option value="Universidad">Universidad</option>
                  </select>
              </div>

              <div class="form-group col-lg-3 col-xs-12">
                  <select class="form-control select-form" id="q" name="q" required="required">
                    <option value="">Seleccione una comuna</option>
                  </select>
              </div>
              
              <div class="form-group col-lg-3 col-xs-12">
                  <select class="form-control select-form" name="c" id="c" required="required">
                    <option value="">Seleccione una carrera</option>
                  </select>
              </div>

              <div class="form-group col-lg-2 col-xs-12">
                  <select class="form-control select-form" id="j" name="j" required="required">
                    <option value="">Jornada</option>
                    <option value="Diurno">Diurno</option>
                    <option value="Otro">Otro</option>
                    <option value="Vespertino">Vespertino</option>
                  </select>
              </div>

              <div class="col-lg-2 col-xs-12">
                <button type="submit" class="btn btn-success btn-submit" style="margin-bottom: 0px;">Verificar <i class="fa fa-check"></i></button>
              </div>              

            </form>
          </div>
        </div>
      </div>
     
      <div class="row">           
        <div class="jumbotron text-center">
          <h2 class="display-3">¡Bienvenido!</h2>
          <p class="lead">Elige las opciones en el formulario y entérate de la información de las carreras de Educación Superior por ciudad, nombre y jornada.</p>
        </div>
      </div>     
    </div>

    @include('footer.index')

      <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="js/jquery.loading.min.js"></script>
    
    <script type="text/javascript">

      $(document).ready(function() {
          $('.btn-submit').click(function(){
              if ($( "#q" ).val() && $( "#t" ).val() && $( "#c" ).val() && $( "#j" ).val()) {  
                  $("body").loading({
                      theme: 'dark',
                      message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Consultando...</h3>'
                  });   
              }
          });

          $(".select-form").select2({  width: '100%' });

          $('#t').on('change', function(event) {

              $("body").loading({
                  theme: 'dark',
                  message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Buscando comunas...</h3>'
              });

              $.ajax({
                  url: 'getDataPrograma',
                  type: 'GET',
                  data: {tipo_institucion: $(this).val()},
              })
              .done(function(response) {
                  var options = ''
                  options += `<option value="">Seleccione una comuna</option><option value="todas">Todas las comunas</option>`
                  $.each(response, function(key, value){
                      options += `<option value="${value.nombre_sede}">${value.nombre_sede}</option>`
                  })
                  $('#q').html(options);
                  $("body").loading('stop');
              })
              .fail(function() {
                  console.log("error");
              })
          });

          $('#q').on('change', function(event) {

              $("body").loading({
                  theme: 'dark',
                  message: '<br><h3><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i><br>Buscando carreras...</h3>'
              });

              $.ajax({
                  url: 'getData',
                  type: 'GET',
                  data: {comuna: $(this).val(), tipo_institucion: $('#t').val()},
              })
              .done(function(response) {
                  var options = ''
                  options += `<option value="">Seleccione una carrera</option>`
                  $.each(response, function(key, value){
                      options += `<option value="${value.nombre_programa}">${value.nombre_programa}</option>`
                  })
                  $('#c').html(options);
                  $("body").loading('stop');
              })
              .fail(function() {
                  console.log("error");
              })
          });
      });
    </script>
  </body>
</html>
