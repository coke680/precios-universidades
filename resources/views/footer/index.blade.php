<footer class="text-center">
  <div class="container">
    <p>Datos oficiales proporcionados por el Consejo Nacional de Educación.</p>
    Creado con <i class="fa fa-heart fa-btn text-danger"></i> por <a target="_blank" href="https://coke680.bitbucket.io">Jorge Martínez</a>
    <p>Versión BETA</p>
  </div>
  <div class="clearfix"></div>
</footer>