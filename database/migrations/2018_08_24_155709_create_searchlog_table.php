<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchlogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('t')->default(0);
            $table->string('q')->default(0);
            $table->string('c')->default(0);
            $table->string('j')->default(0);
            $table->string('search_success');
            $table->string('ip_client_address')->default(0); 
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searchlogs');
    }
}
