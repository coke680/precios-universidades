<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Searchlog extends Model
{
    protected $fillable = [
        't', 'q', 'c', 'j', 'search_success', 'ip_client_address'
    ];
}
