<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Session;

class CommentsController extends Controller
{
	public function store(Request $request) {

	    $this->validate($request, [
	        'institucion' => 'required',
	        'city' => 'required',
	        'career' => 'required',
	        'time' => 'required',
	        'comment' => 'required',
	    ]);

	    $comment = new Comment();
	   // Assuming its not mass assignable
	    $comment->institucion = $request->input('institucion');
	    $comment->city = $request->input('city');
	    $comment->career = $request->input('career');
	    $comment->time = $request->input('time');
	    $comment->comment = $request->input('comment');
	    $comment->save(); 

	    Session::flash('success', '¡Tu comentario ha sido enviado! Gracias :)');

    	return redirect()->back();
	}
}
