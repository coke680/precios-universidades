<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Searchlog;
use Session;

class WelcomeController extends Controller
{
    public function index(Request $request) {       


        if ($request->t && $request->c && $request->j && $request->q) {

            $this->validate($request, [
                't' => 'required',
                'q' => 'required',
                'c' => 'required',
                'j' => 'required',
            ]);

            if ($request->q == 'todas') {

                $data_chart = DB::select("select nombre_sede, duracion, valor_matricula, valor_titulo, clasificacion_3, clasificacion_5, nombre_programa, nombre_institucion, tipo_institucion_fixed, avg(valor_arancel) as valor_arancel, matricula_primer, ultimo_corte_psu from precios where tipo_institucion_fixed = '{$request->t}' and nombre_programa = '{$request->c}' and year = 2018 and jornada = '{$request->j}' group by tipo_institucion_fixed, nombre_programa, nombre_sede, matricula_primer, nombre_institucion, valor_titulo, valor_matricula, clasificacion_3, clasificacion_5, duracion, ultimo_corte_psu order by valor_arancel desc");

            }

            else {

                $data_chart = DB::select("select nombre_sede, duracion, valor_matricula, valor_titulo, clasificacion_3, clasificacion_5, nombre_programa, nombre_institucion, tipo_institucion_fixed, avg(valor_arancel) as valor_arancel, matricula_primer, ultimo_corte_psu from precios where tipo_institucion_fixed = '{$request->t}' and nombre_sede = '{$request->q}' and nombre_programa = '{$request->c}' and year = 2018 and jornada = '{$request->j}' group by tipo_institucion_fixed, nombre_programa, nombre_sede, matricula_primer, nombre_institucion, valor_titulo, valor_matricula, clasificacion_3, clasificacion_5, duracion, ultimo_corte_psu order by valor_arancel desc");
            }            

            if (count($data_chart) == 0) {
                Searchlog::create(array_add($request->all(), 'search_success', '0'));
            }
            else {
                Searchlog::create(array_add($request->all(), 'search_success', '1'));
            }            

            return view('welcome', ['data_chart' => $data_chart]);
            
        } else {

            return view('intro');
            
        }

    }

    public function getData(Request $request) {

        if ($request->comuna == 'todas') {

            return DB::table('precios')->select('nombre_programa')
                    ->where('tipo_institucion_fixed', $request->tipo_institucion)
                    ->where('year', 2018)
                    ->groupby('nombre_programa')
                    ->orderBy('nombre_programa')
                    ->get();
        }
        else {

            return DB::table('precios')->select('nombre_programa')
                    ->where('tipo_institucion_fixed', $request->tipo_institucion)
                    ->where('nombre_sede', $request->comuna)
                    ->where('year', 2018)
                    ->groupby('nombre_programa')
                    ->orderBy('nombre_programa')
                    ->get();
        }
    }

    public function getDataPrograma(Request $request) {
        
        return DB::table('precios')->select('nombre_sede')
                ->where('tipo_institucion_fixed', $request->tipo_institucion)
                ->where('year', 2018)
                ->groupby('nombre_sede')
                ->orderBy('nombre_sede')
                ->get();
    }

}
